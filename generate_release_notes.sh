#!/usr/bin/env bash

version=$(awk -F "[<>]" '/SNAPSHOT/{gsub(/-SNAPSHOT/,"",$3);print $3}' pom.xml)
since="JAN 6 2020"
until="JAN 12 2020"
source="git log --since \"$since\" --until \"$until\" --pretty='%h%x09%an%x09%s'"
#source="cat ./testdata"
date=$(date +'%A %d/%m/%Y')

function insertText() {

  echo -e "# Release Notes 
## Palantir v$version
$date

Palantir is a web service that provides data about conflicts around the world. Data is mined from [Conflict News](https://twitter.com/conflicts). 

### Installation and Upgrade Notes
[Download palantir-$version]()

Extract **palantir-v$version.tar** and run \`java -jar palantir-v$version.jar\`
**OR** install Docker, build the docker image by running \`docker build . -t \"palantir-v$version\" -f Dockerfile\` inside root dir and run \`docker run -d palantir-v$version:latest\`.

OpenJDK 11 is required to run the .JAR.\n" >>RELEASE.md
}

function generateNotes() {
  readarray -t lines < <(eval $source | grep Closes | grep -v Bugfix)

  echo -e "### Closed Issues\n\n" >>RELEASE.md

  for each in "${lines[@]}"; do
    commitMessage=$(echo "$each" | awk -F "[\"]" '{print $(NR+3)}' | xargs)
    issueNumber=$(echo "$each" | awk -F "[\"]" '{print $(NR+2)}' | xargs)
    issueType=$(echo "$each" | awk -F "[\"]" '{print $(NR+1)}' | xargs)
    echo -e "[$issueType - $issueNumber](https://gitlab.com/serres-parc/palantir/issues/$issueNumber) - $commitMessage\n" >>RELEASE.md
  done

  echo -e "\n####  Bugfixes\n\n" >>RELEASE.md

  readarray -t bugs < <(eval $source | grep Bugfix)

  for each in "${bugs[@]}"; do
    commitMessage=$(echo "$each" | awk -F "[\"]" '{print $(NR+3)}' | xargs)
    issueNumber=$(echo "$each" | awk -F "[\"]" '{print $(NR+2)}' | xargs)
    issueType=$(echo "$each" | awk -F "[\"]" '{print $(NR+1)}' | xargs)
    echo -e "* [$issueType - $issueNumber](https://gitlab.com/serres-parc/palantir/issues/$issueNumber) - $commitMessage\n" >>RELEASE.md
  done

  echo -e "\n####  Contributors\n\n" >>RELEASE.md

  readarray -t contributors < <(eval $source | grep Closes | awk -F " " '{print $2" "$3}' | sort -u | uniq)

  echo -e "The following people contributed to **v$version**.\n" >>RELEASE.md

  for contributor in "${contributors[@]}"; do
    readarray -t contributions < <(eval $source | grep Closes | grep "$contributor")
    echo "##### $contributor" >>RELEASE.md
    for contribution in "${contributions[@]}"; do
      issueNumber=$(echo "$contribution" | awk -F "[\"]" '{print $(NR+2)}' | xargs)
      echo -e "* [x]  Contribution [$issueNumber](https://gitlab.com/serres-parc/palantir/issues/$issueNumber)" >>RELEASE.md
    done
    sed -i -e '$a' ./RELEASE.md
  done
}

function createTarball() {
  tar cf palantir-"$version".tar -C ./target/ palantir-v"$version"-SNAPSHOT.jar
  tar rf palantir-"$version".tar RELEASE.md README.md LICENSE
  sed -i "8s/()/(palantir-v$version.tar)/" RELEASE.md
}
insertText
generateNotes
createTarball
