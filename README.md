## Palantir
[![pipeline status](https://gitlab.com/serres-parc/palantir/badges/master/pipeline.svg)](https://gitlab.com/serres-parc/palantir/commits/master)  [![coverage report](https://gitlab.com/serres-parc/palantir/badges/master/coverage.svg)](https://gitlab.com/serres-parc/palantir/commits/master)

A service running periodically to mine tweets from [Conflicts](https://twitter.com/conflicts),
extract valuable information and present it in a nice front-end.

[Click for live demo](http://project-palantir.herokuapp.com/)

##### Palantir showing conflict in clusters.
![](palantir-demo1.jpg)

##### Palantir correctly identified an explosion in Nigeria.
![](palantir-demo2.jpg)

##### Palantir demo of hover event.
![hover demo](palantir-demo3.gif)

##### Palantir  zoom in on conflict cluster.
![cluster zoom in demo](palantir-demo4.gif)

