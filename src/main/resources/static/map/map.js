// set up map with centering coordinates
var map = L.map('map', {
    minZoom: 3,
    maxZoom: 16,
    worldCopyJump: false,
    maxBounds: [
        //south west
        [-90, -180],
        //north east
        [90, 180]
    ],

}).setView([39.90974, 2.81250], 3);

var gl = L.mapboxGL({
    attribution: '<a href="https://carto.com/" target="_blank">© CARTO</a> <a href="https://www.maptiler.com/copyright/" target="_blank">© MapTiler</a> <a href="https://www.openstreetmap.org/copyright" target="_blank">© OpenStreetMap contributors</a>',
    accessToken: 'not-needed',
    style: 'https://api.maptiler.com/maps/voyager/style.json?key=ckTGuBCAt5wTMljaEVhP'
}).addTo(map);

var markers = L.markerClusterGroup({
    maxClusterRadius: function (zoom) {
        return (zoom <= 14) ? 30 : 1; // radius in pixels
    },
    iconCreateFunction: function (cluster) {
        var childCount = cluster.getChildCount();
        var c = ' marker-cluster-';
        if (childCount < 3) {
            c += 'small';
        } else if (childCount < 5) {
            c += 'medium';
        } else {
            c += 'large';
        }
        return new L.DivIcon({
            html: '<div><span>' + childCount + '</span></div>',
            className: 'marker-cluster' + c,
            iconSize: new L.Point(40, 40)
        });
    }
});

var warningIcon = L.icon({
    iconUrl: '/map/warning1.png',
    iconSize: [30, 30]
});

let conflictsJson;
fetch('Conflicts').then(
    function (u) {
        return u.json();
    }
).then(
    function (json) {
        conflictsJson = json;
        var conflictMarkers = (function () {
            for (let index = 0; index < conflictsJson.length; index++) {
                var lat = conflictsJson[index].latitude;
                var lon = conflictsJson[index].longitude;

                console.log(lat, " ", lon);
                var popUpContent = conflictsJson[index].embeddedHtml;
                var toolTipContent = "Location: " + conflictsJson[index].location + " <br>" + conflictsJson[index].typeOfConflict;
                markers.addLayer(new L.marker(new L.LatLng(lat, lon), {
                    icon: warningIcon
                    // radius: setRadius(conflictsJson, conflictsJson[index].location),
                    // radius: 11,
                    // color: '#FF0000',
                    // weight: 4,
                    // opacity: 0.5,
                    // fillOpacity: 0.25
                }).bindPopup(popUpContent, {maxWidth: 220}).bindTooltip(toolTipContent, {
                    permanent: false,
                    direction: "top",
                    className: "my-labels"
                }));
            }
            map.addLayer(markers);
        })();
    }
).catch((e) => console.error(e));

// function setRadius(Conflicts, name) {
//     let radius = 1;
//
//     for (let index = 0; index < Conflicts.length; index++) {
//         if (Conflicts[index].location === name) {
//             radius++;
//         }
//     }
//     return radius * 5;
// }

