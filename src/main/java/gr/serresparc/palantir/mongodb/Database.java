package gr.serresparc.palantir.mongodb;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoDatabase;
import org.springframework.context.annotation.Configuration;

import java.util.logging.Logger;

/**
 * Class for MongoDB configuration
 */
@Configuration
public class Database
{
    private static final Logger logger = Logger.getLogger(Database.class.getName());
    private final static String DATABASE = "";
    MongoClient mongoClient;
    MongoDatabase mongoDatabase;

    /**
     * Connects to a MongoDB schema and retutns a MongoDatabase
     *
     * @return MongoDatabase
     */
    public MongoDatabase connectToAtlas()
    {
        MongoClientURI uri = new MongoClientURI("");
        mongoClient = new MongoClient(uri);
        try
        {
            mongoDatabase = mongoClient.getDatabase(Database.DATABASE);
            logger.info("Connection to Atlas was successful!");
            return mongoDatabase;
        }
        catch (Exception e)
        {
            logger.severe("Connection to Atlas failed! Error: " + e);
            return null;
        }
    }

    /**
     * Returns a configured MongoClient
     *
     * @return MongoClient
     */
    public MongoClient getMongoClient()
    {
        return mongoClient;
    }
}
