package gr.serresparc.palantir.service;

import com.google.maps.model.LatLng;
import gr.serresparc.palantir.dao.ConflictDAO;
import gr.serresparc.palantir.model.Conflict;
import gr.serresparc.palantir.model.Tweet;
import gr.serresparc.palantir.parser.Parser;
import gr.serresparc.palantir.repository.Conflicts;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import twitter4j.Twitter;
import twitter4j.TwitterException;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.logging.Logger;

/**
 * Service class that implements Runnable and sends Conflict documents to MongoDB
 */
@Service
public class ConflictService implements Runnable
{
    private static final Logger logger = Logger.getLogger(ConflictService.class.getName());
    private static Conflicts listOfConflicts = new Conflicts();
    private String conflictType = "";
    private final Twitter twitter;
    private final Parser parser;
    private final CoordinatesService coordinatesService;
    private final ConflictDAO conflictDAO;

    /**
     * Initialization of Service class
     *
     * @param twitter            Twitter
     * @param parser             Parser
     * @param coordinatesService CoordinatesService
     * @param conflictDAO        ConflictDAO
     */
    @Autowired
    public ConflictService(
            Twitter twitter,
            Parser parser,
            CoordinatesService coordinatesService,
            ConflictDAO conflictDAO)
    {
        this.twitter            = twitter;
        this.parser             = parser;
        this.coordinatesService = coordinatesService;
        this.conflictDAO        = conflictDAO;
    }

    /**
     * Generates Conflict documents from a Twitter handle and sends the resulting object to a database
     */
    public void run()
    {
        listOfConflicts.setlistOfConflicts();
        logger.info("Palantir backend initializing!");
        try
        {
            List<Tweet> newTweetList = parser.parseJSON(twitter.getUserTimeline("@Conflicts"), twitter);
            for (Tweet newTweet : newTweetList)
            {
                String location;
                if (parser.isConflict(newTweet.getTweetText()) && !conflictDAO.hasTweet(newTweet.getId()))
                {
                    List<String> textList = parser.statusTextToParse(newTweet);
                    location = parser.locationFound(newTweet.getTweetText());
                    conflictType = listOfConflicts.getTypeOfConflict(listOfConflicts.findKeyWords(textList));
                    LatLng latLng = coordinatesService.getLatLng(location);
                    if (location != null && latLng != null)
                    {
                        conflictDAO.create(new Conflict(new Random().nextLong(),
                                                        location,
                                                        conflictType,
                                                        "https://twitter.com/Conflicts/status/" + newTweet.getId(),
                                                        latLng.lat,
                                                        latLng.lng,
                                                        newTweet.getTimeStamp(),
                                                        newTweet.getEmbeddedHtml()
                        ));
                        logger.info("New conflict detected in " + location + " and sent to database!");
                        try
                        {
                            Thread.sleep(3_000L);
                        }
                        catch (InterruptedException e)
                        {
                            e.printStackTrace();
                        }
                    }
                    else
                    {
                        logger.info("Location could not be determined!");
                    }
                }
                else
                {
                    logger.info("Tweet is not considered a conflict or is already in database \"" + newTweet.getTweetText() + "\"");
                }
            }
        }
        catch (TwitterException e)
        {
            logger.severe("Twitter error! Error: " + e);
        }
    }
}
