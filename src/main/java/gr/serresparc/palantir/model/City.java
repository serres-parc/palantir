package gr.serresparc.palantir.model;

public class City
{
    private String country;
    private String name;
    private String subcountry;

    public String getSubcountry()
    {
        return subcountry;
    }

    private String geonameid;
    private double latitude;
    private double longitude;

    City(String country, String geonameid, String name, String subcountry)
    {
        this.country    = country;
        this.geonameid  = geonameid;
        this.name       = name;
        this.subcountry = subcountry;
    }

    City(String name, double latitude, double longitude)
    {
        this.name      = name;
        this.latitude  = latitude;
        this.longitude = longitude;
    }

    public String getCountry()
    {
        return country;
    }

    public String getName()
    {
        return name;
    }

}
