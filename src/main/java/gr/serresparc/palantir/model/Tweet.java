package gr.serresparc.palantir.model;

import java.util.Date;

public class Tweet
{
    private String tweetText;
    private long id;
    private Date timeStamp;
    private int numberOfRetweets;
    private String embeddedHtml;

    /**
     * A constructor for the class Tweet
     */
    public Tweet(String tweetText, long id, Date timeStamp, int numberOfRetweets, String embeddedHtml)
    {
        this.tweetText        = tweetText;
        this.id               = id;
        this.timeStamp        = timeStamp;
        this.numberOfRetweets = numberOfRetweets;
        this.embeddedHtml     = embeddedHtml;
    }

    public String getTweetText()
    {
        return tweetText;
    }

    public long getId()
    {
        return id;
    }

    public Date getTimeStamp()
    {
        return timeStamp;
    }

    public void setTimeStamp(Date timeStamp)
    {
        this.timeStamp = timeStamp;
    }

    public String getEmbeddedHtml() {return embeddedHtml;}

    public int getNumberOfRetweets()
    {
        return numberOfRetweets;
    }


}
