package gr.serresparc.palantir.model;

public class Country
{

    private double latitude;
    private double longitude;
    private String name;
    private long conflictsCount;
    private String code;

    public String getCode()
    {
        return code;
    }

    public Country(double latitude, double longitude, String name, long conflictsCount)
    {
        this.latitude       = latitude;
        this.longitude      = longitude;
        this.name           = name;
        this.conflictsCount = conflictsCount;
    }


    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public double getLatitude()
    {
        return latitude;
    }

    public double getLongitude()
    {
        return longitude;
    }

    public long getConflictsCount() {
        return conflictsCount;
    }
}
