package gr.serresparc.palantir.repository;

import gr.serresparc.palantir.filter.ClearString;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * Conflict type resolution repository class
 */
public class Conflicts
{

    private static HashMap<String, String> conflictIdent = new HashMap<>();
    private static HashMap<String, String> conflictActions = new HashMap<>();
    private static List<String> conflictResponsibles = new ArrayList<>(Arrays.asList("militia","militias","soldier","soldiers","civilian","civilians","government","governments"));
    private List<String> existingResponsible = new ArrayList<>();
    private String typeOfConflict = "Unknown conflict";
    private String responsible = "Unknown party";
    private String action ="Unknown action";
    private ClearString matcher = new ClearString();
    /**
     * Initializer method for conflict type resolution
     * at the moment only :
     * Civil War/War
     * Aerial Combat
     * Conflict
     * Killing
     * Death
     * Armed Conflict
     * Ship Sinking
     * IED Bombing
     * Injuries
     * Aircraft/Land Vehicle Crash
     * Fires
     * Lynching
     * Shootouts
     * Execution
     * Bombing
     * and Aerial Bombing
     * are recognized as Conflict Types
     */
    public void setlistOfConflicts()
    {

        //Setup Conflicts
        conflictIdent.put("war", "Civil War/War");
        conflictIdent.put("dogfights", "Aerial Combat");
        conflictIdent.put("dogfight", "Aerial Combat");
        conflictIdent.put("conflict", "Conflict");
        conflictIdent.put("conflicts", "Conflict");
        conflictIdent.put("killing", "Killing");
        conflictIdent.put("killings", "Killing");
        conflictIdent.put("dead", "Death(s)");
        conflictIdent.put("death", "Death(s)");
        conflictIdent.put("attack", "Armed Conflict(s)");
        conflictIdent.put("attacked", "Armed Conflict(s)");
        conflictIdent.put("attacking", "Armed Conflict(s)");
        conflictIdent.put("sink", "Ship Sinking");
        conflictIdent.put("sunk", "Ship Sinking");
        conflictIdent.put("shoot", "Armed Conflict(s)");
        conflictIdent.put("shot", "Armed Conflict(s)");
        conflictIdent.put("exploded", "IED Bombing(s)");
        conflictIdent.put("explosion", "IED Bombing(s)");
        conflictIdent.put("injuries", "Injury/Injuries");
        conflictIdent.put("injured", "Injury/Injuries");
        conflictIdent.put("crashed", "Aircraft/Land Vehicle Crash");
        conflictIdent.put("crash", "Aircraft Crash");
        conflictIdent.put("shooting", "Armed Conflict(s)");
        conflictIdent.put("shootings", "Armed Conflict(s)");
        conflictIdent.put("fired", "Armed Conflict(s)");
        conflictIdent.put("fire", "Fire(s)");
        conflictIdent.put("died", "Death(s)");
        conflictIdent.put("killed", "Death(s)");
        conflictIdent.put("lynching", "Lynching");
        conflictIdent.put("lynched", "Lynching");
        conflictIdent.put("shootout", "Shootout(s)");
        conflictIdent.put("beheads", "Execution");
        conflictIdent.put("beheading", "Execution");
        conflictIdent.put("beheaded", "Execution");
        conflictIdent.put("execution", "Execution");
        conflictIdent.put("gallows", "Execution");
        conflictIdent.put("bombs", "Bombing(s)");
        conflictIdent.put("bomb", "IED Bombing(s)");
        conflictIdent.put("explosions", "IED Bombing(s)");
        conflictIdent.put("bombing", "IED Bombing(s)");
        conflictIdent.put("bombings", "IED Bombing(s)");
        conflictIdent.put("bombed", "Aerial Bombing(s)");
        conflictIdent.put("open fire","opened fire");
        conflictIdent.put("opened fire","opened fire");
        conflictIdent.put("opens fire","opened fire");
        conflictIdent.put("revolution","revolution");

        ////Setup Actions
        conflictActions.put("open","opened");
        conflictActions.put("opened","opened");
        conflictActions.put("opens","opened");
    }

    /**
     * Method for determining conflict type based on a HashMap
     *
     * @param text List<String>
     * @return String
     */
    public String getTypeOfConflict(String text)
    {
        typeOfConflict = text;
        if(!action.equalsIgnoreCase("Unknown action")){
            typeOfConflict = matcher.stringMatcher(action,typeOfConflict);
        }

        if (conflictIdent.containsKey(typeOfConflict))
        {
            typeOfConflict = conflictIdent.get(typeOfConflict);
        }
        return "Responsible: " + responsible +"<br>"+"Type: " +typeOfConflict;
    }

    public String findKeyWords(List<String> text){

        for(String s : text){
            if (conflictActions.containsKey(s)){
                action = conflictActions.get(s);
            }
            else if(conflictResponsibles.contains(s)){
                existingResponsible.add(s);
            }
            else if(conflictIdent.containsKey(s)){
                typeOfConflict = s;
            }
        }
        if (!existingResponsible.isEmpty()){
            responsible = existingResponsible.get(0);
        }
        return typeOfConflict;
    }


    public String getResponsible() {
        return responsible;
    }

    public String getAction() {
        return action;
    }

    public HashMap<String, String> getConflictIdent() {
        return conflictIdent;
    }
}
