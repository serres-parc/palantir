package gr.serresparc.palantir.service;

import com.google.maps.model.LatLng;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Spy;
import org.springframework.test.util.ReflectionTestUtils;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

class CoordinatesServiceTest
{
    @Spy
    private CoordinatesService coordinatesService = new CoordinatesService();

    @BeforeEach
    public void setUp()
    {
        ReflectionTestUtils.setField(coordinatesService, "API_KEY", "");
    }

    @Test
    void searchForCoordinatesThessaloniki()
    {
        LatLng latLng = coordinatesService.getLatLng("Thessaloniki");
        assertEquals(40.64006290, latLng.lat);
        assertEquals(22.94441910, latLng.lng);
    }

    @Test
    void searchForCoordinatesAthens()
    {
        LatLng latLng = coordinatesService.getLatLng("Moscow");
        assertEquals(55.75582600, latLng.lat);
        assertEquals(37.61729990, latLng.lng);
    }

    @Test
    void searchForCoordinatesBarista()
    {
        LatLng latLng = coordinatesService.getLatLng("Barista");
        assertNull(latLng);
    }

    @Test
    void searchForCoordinatesChina()
    {
        LatLng latLng = coordinatesService.getLatLng("China");
        assertEquals(35.86166000, latLng.lat);
        assertEquals(104.19539700, latLng.lng);
    }

    @Test
    void searchForCoordinatesAthensGreece()
    {
        LatLng latLng = coordinatesService.getLatLng("Thessaloniki,Greece");
        assertEquals(40.64006290, latLng.lat);
        assertEquals(22.94441910, latLng.lng);
    }

    @Test
    void searchForCoordinatesSyriaza()
    {
        LatLng latLng = coordinatesService.getLatLng("Syriaza");
        assertNull(latLng);
    }

    @Test
    void searchForCoordinatesSyria()
    {
        LatLng latLng = coordinatesService.getLatLng("Syria");
        assertEquals(34.80207499999999, latLng.lat);
        assertEquals(38.99681500, latLng.lng);
    }

    @Test
    void searchForCoordinatesMALI()
    {
        LatLng latLng = coordinatesService.getLatLng("MALI");
        assertEquals(17.57069200, latLng.lat);
        assertEquals(-3.99616600, latLng.lng);
    }

    @Test
    void searchForCoordinatesNeapoli()
    {
        LatLng latLng = coordinatesService.getLatLng("Neapoli");
        assertNull(latLng);
    }

    @Test
    void searchForCoordinatesVladimir()
    {
        LatLng latLng = coordinatesService.getLatLng("Vladimir");
        assertEquals(56.14459560, latLng.lat);
        assertEquals(40.41786870, latLng.lng);
    }
}