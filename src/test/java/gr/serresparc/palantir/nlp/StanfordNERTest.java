package gr.serresparc.palantir.nlp;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@Disabled
class StanfordNERTest
{
    StanfordNER stanfordNER = new StanfordNER();


    @Test
    void getLocationWithNLP1()
    {
        String testString1 = "MALI was taken over by enemy forces";
        String result = stanfordNER.extractLocation(testString1);

        assertEquals("MALI", result);

    }

    @Test
    void getLocationWithNLP2()
    {
        String result = stanfordNER.extractLocation("BREAKING: A new into Idlib");
        assertEquals("Idlib", result);
    }

    @Test
    void getLocationWithNLP3()
    {
        String result = stanfordNER.extractLocation("Moscow");
        assertEquals("Moscow", result);
    }

    @Test
    void getLocationWithNLP4()
    {
        String result = stanfordNER.extractLocation("Libya");
        assertEquals("Libya", result);
    }

    @Test
    void getLocationWithNLP5()
    {
        String result = stanfordNER.extractLocation("Niger");
        assertEquals("Niger", result);
    }

    @Test
    void getLocationWithNLP6()
    {
        String tweet = "BREAKING: Trump to give address on Iran at 11:00 am (1600 GMT)";
        String result = stanfordNER.extractLocation(tweet);
        assertEquals("Iran", result);
    }

    @Test
    void getLocationWithNLP7()
    {
        String tweet = "CORRECTION: Earlier reports that the Kuwait MoD had received a letter " +
                       "saying that US troops were pulling out appear to be false and based on a hack.";

        String result = stanfordNER.extractLocation(tweet);
        assertEquals("Kuwait", result);

    }

    @Test
    void getLocationWithNLP8()
    {
        String tweet = "VIDEO: Iranian media release more footage of the moment missiles " +
                       "were fired at the Ayn al Asad military base in Iraq.";

        String result = stanfordNER.extractLocation(tweet);
        assertEquals("Iraq", result);

    }

    @Test
    void getLocationWithNLP9()
    {
        String tweet = "BREAKING: Official statement from Iraqi Prime Minister states that Iran " +
                       "gave warning of the attack against military bases shortly after midnight last night.";

        String result = stanfordNER.extractLocation(tweet);
        assertEquals("Iran", result);

    }

    @Test
    void getLocationWithNLP10()
    {
        String tweet = "Final altitude, speed, and location tracked of Ukrainian Airlines" +
                       " flight PS752 that crashed shortly after take off in Tehran.";

        String result = stanfordNER.extractLocation(tweet);
        assertEquals("Tehran", result);

    }

    @Test
    void getLocationWithNLP11()
    {
        String tweet = "UPDATE: Ukrainian President Volodymyr Zelenskiy says " +
                       "he has instructed prosecutors to open criminal proceedings " +
                       "after a Ukraine International Airlines flight with 176 people " +
                       "on board crashed shortly after taking off from Tehran - @SkyNewsBreak";

        String result = stanfordNER.extractLocation(tweet);
        assertEquals("Tehran", result);

    }

    @Test
    void getLocationWithNLP12()
    {
        String tweet = "IRAQ: We are monitoring reports of Iraqi casualties as a result of Iranian missile strikes. " +
                       "There is no information as of yet on whether there were American casualties.";

        String result = stanfordNER.extractLocation(tweet);
        assertEquals("IRAQ", result);
    }

    @Test
    void getLocationWithNLP14()
    {
        String tweet = "United Kingdom attacked US .";

        String result = stanfordNER.extractLocation(tweet);
        assertEquals("United Kingdom", result);

    }

    @Test
    void getLocationWithNLP15()
    {
        String tweet = "BREAKING: Mortar shells have landed inside Baghdad, Iraq";

        String result = stanfordNER.extractLocation(tweet);
        assertEquals("Baghdad", result);
    }

    //Great success bug 54 is no more
    @Test
    void getLocationWithNLP21()
    {
        String tweet = "BREAKING: Russian President Vladimir Putin and Ukrainian President Volodymyr Zelensky  agree " +
                       "full ceasefire in eastern Ukraine by year-end - @dpa_intl";

        String result = stanfordNER.extractLocation(tweet);
        assertEquals("Ukraine", result);
    }

    @Test
    void getLocationWithNLP16()
    {
        String tweet = "VIDEO: Iranian media release more footage of the moment missiles " +
                       "were fired at the Ayn al Asad military base in Iraq.";

        String result = stanfordNER.extractLocation(tweet);
        assertEquals("Iraq", result);

    }

    @Disabled
    @Test
    void getLocationWithNLP17()
    {
        String tweet = "SYRIA: The Iranian militia group the 'Imam Ali' brigade report they were struck by 2 aircraft" +
                       " in al-Bukamal on the Syria/Iraq border.";

        String result = stanfordNER.extractLocation(tweet);
        assertEquals("SYRIA", result);
    }

    @Test
    void getLocationWithNLP18()
    {
        String tweet = "BREAKING: The Sultan of Oman, Qaboos bin Said, passed away early on Saturday - @MiddleEastEye";

        String result = stanfordNER.extractLocation(tweet);
        assertEquals("Oman", result);
    }

    @Test
    void getLocationWithNLP19()
    {
        String tweet = "We're looking at getting our website back up and running hopefully in the near future. What " +
                       "kind of things would you like to see on there? We've previously had live blogs of breaking " +
                       "news as well as analytical pieces of world events. Suggestions welcome!";

        String result = stanfordNER.extractLocation(tweet);
        assertNull(result);

    }

    @Test
    void getLocationWithNLP20()
    {
        String tweet = "IRAN: Large protests have erupted across the country today as anger at the government in the " +
                       "wake of the #PS572 shootdown increases.";

        String result = stanfordNER.extractLocation(tweet);
        assertEquals("IRAN", result);

    }
}










