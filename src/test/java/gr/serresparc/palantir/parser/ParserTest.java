package gr.serresparc.palantir.parser;


import gr.serresparc.palantir.model.Tweet;
import gr.serresparc.palantir.nlp.StanfordNER;
import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;

class ParserTest
{
    StanfordNER locationExtractor = mock(StanfordNER.class);

    Parser parser = new Parser(locationExtractor);

    int numberOfRetweets = 35;
    long idTweet = 123456779L;
    long idNewTweet = 123456778L;
    Tweet tweet = new Tweet("This is an example String for tweet.getTweetText()", idTweet, null, numberOfRetweets,
                            null);
    Tweet newTweet = new Tweet("This is an example String for tweet.getTweetText()", idNewTweet, null,
                               numberOfRetweets, null);
    List<Tweet> tweetList = new ArrayList<>();

    @Test
    void testIsConflict1()
    {
        boolean actual = parser.isConflict("VIDEO: Footage allegedly shows the moment a missile fired from Iran " +
                                           "strikes the Ayn al Asad base used by US forces in Iraq.");
        assertTrue(actual);
    }

    @Test
    void testIsConflict2()
    {
        boolean actual = parser.isConflict("BREAKING: US Department of Defense issues statement in response to " +
                                           "attacks on bases in Iraq - @adegrandpre");
        assertTrue(actual);
    }

    @Test
    void testIsConflict3()
    {
        boolean actual = parser.isConflict("BREAKING: Pentagon confirms that both Erbil and Al-Asad Airbase were hit " +
                                           "with ballistic missiles fired by Iran.");
        assertTrue(actual);
    }

    @Test
    void testIsConflict4()
    {
        boolean actual = parser.isConflict("BREAKING: Iran threatens 'more crushing responses' if fresh US attack: " +
                                           "state television - @AFP");
        assertTrue(actual);
    }


    @Test
    void testIsConflict5()
    {
        boolean actual = parser.isConflict("IRAQ: These photos are NOT from the attack in Iraq tonight. The first is " +
                                           "from Gaza in Nov 2019 and the second is from Syria in 2018. There is a " +
                                           "lot of false information going about tonight. We will do our very best to" +
                                           " only provide accurate information.");
        assertTrue(actual);
    }

    @Test
    void testIsConflict6()
    {
        boolean actual = parser.isConflict("BREAKING: White House press secretary says President Trump has been " +
                                           "briefed on the situation in Iraq and is monitoring the situation closely " +
                                           "@axios.");
        assertFalse(actual);
    }

    @Test
    void testIsConflict7()
    {
        boolean actual = parser.isConflict("BREAKING: Iranian Revolutionary Guard Corps confirms hitting US Ain Al " +
                                           "Assad Airbase in #Iraq with tens of missiles. - @PressTV");
        assertTrue(actual);
    }

    @Test
    void testIsConflict8()
    {
        boolean actual = parser.isConflict("VIDEO: @FarsNews_Agency posts unverified video claimed to be of missiles " +
                                           "being launched towards Al Asad Airbase in Iraq.https://twitter" +
                                           ".com/FarsNews_Agenc");
        assertTrue(actual);
    }

    @Test
    void testIsConflict9()
    {
        boolean actual = parser.isConflict("UPDATE: Multiple reports of explosions also heard in the northern Iraqi " +
                                           "city of Erbil.");
        assertTrue(actual);
    }

    @Test
    void testIsConflict10()
    {
        boolean actual = parser.isConflict("BREAKING: Pentagon correspondent @JackDetsch_ALM\n" +
                                           " says he has confirmation from a defence official that the Ayn al Asad " +
                                           "base in Iraq was hit by 6 rockets this evening.");
        assertTrue(actual);
    }

    @Test
    void testIsConflict11()
    {
        boolean actual = parser.isConflict("UPDATE: US military has reportedly confirmed that the letter is real to a" +
                                           " Washington Post journalist.");
        assertFalse(actual);
    }

    @Test
    void testIsConflict12()
    {
        boolean actual = parser.isConflict("BREAKING: Letter from US Department of Defence purportedly shows a US " +
                                           "intent to move out of Iraq. \n" +
                                           "\n" +
                                           "The veracity of the letter is currently unknown.");
        assertFalse(actual);
    }

    @Test
    void testIsConflict13()
    {
        boolean actual = parser.isConflict("BREAKING: Iranian state TV reports Iran will no longer abide by any " +
                                           "limits of its 2015 nuclear deal. http://apne.ws/4Sji74x - @AP");
        assertFalse(actual);
    }

    @Test
    void isNewByDateTest() throws ParseException
    {
        String lastDateRecorded = "Fri Nov 08 11:37:38 EET 2019";
        Date convertedLastDateRecorded =
                new SimpleDateFormat("EEEE MMM dd HH:mm:ss ZZZZZ yyyy", Locale.ENGLISH).parse(lastDateRecorded);
        tweet.setTimeStamp(convertedLastDateRecorded);

        String tweetDate = "Fri Nov 08 11:37:39 EET 2019";//added one second
        Date convertedTweetDate =
                new SimpleDateFormat("EEEE MMM dd HH:mm:ss ZZZZZ yyyy", Locale.ENGLISH).parse(tweetDate);
        newTweet.setTimeStamp(convertedTweetDate);

        // assertEquals(true, tp.isNewByDate(newTweet, tweet));
    }

    @Test
    void isNewByIDTest()
    {
        long newId;
        for (int i = 0; i < 5; i++)
        {
            newId = new Random().nextLong();
            tweetList.add(new Tweet(null, newId, null, 0, null));
        }//For loop to populate a list of Objects Tweet. Main

        assertTrue(parser.isNewById(newTweet, tweetList));


    }


    @Test
    void isNewByIDTestFail()
    {
        long newId;
        for (int i = 0; i < 5; i++)
        {
            newId = new Random().nextLong();
            tweetList.add(new Tweet(null, newId, null, 0, null));
        }//For loop to populate a list of Objects Tweet. Main

        tweetList.add(newTweet);

        assertFalse(parser.isNewById(newTweet, tweetList));
    }

}
