package gr.serresparc.palantir.dao;

import gr.serresparc.palantir.model.Conflict;
import gr.serresparc.palantir.model.Country;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

class ConflictDAOTest
{
    ConflictDAO conflictDAO = new ConflictDAO();


    @Test
    void readConflictsTestByReturningAList()
    {
        List<Conflict> conflicts;
        conflicts = conflictDAO.readConflicts();
        assertNotNull(conflicts);
    }


    @Test
    void conflictsPerCountry()
    {
        List<String> countries = new ArrayList<>();
        countries.add("IRAQ");
        List<Country> countryList = conflictDAO.conflictsPerCountry(countries);
        for (Country country : countryList)
        {
            assertEquals(33.223191d, country.getLatitude(), 0.0d);
            assertEquals(33.223191d, country.getLatitude(), 0.0d);
        }
    }
}